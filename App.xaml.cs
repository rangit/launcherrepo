﻿using System;
using System.Diagnostics;
using System.Windows;
using GalaSoft.MvvmLight.Threading;
using System;
using System.Windows.Threading;
using Microsoft.Win32;
using NLog;

//procdump -t -w app.exe...or possibly procdump -e -w app.exe.
namespace LauncherMvvmLight
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static App()
        {
            //Debugger.Break();
            //Debugger.Launch();
            Get45PlusFromRegistry();
            DispatcherHelper.Initialize();
        }

        private static void Get45PlusFromRegistry()
        {
            const string subkey = @"SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full\";

            using (var ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(subkey))
            {
                if (ndpKey != null && ndpKey.GetValue("Release") != null)
                {
                    Console.WriteLine($".NET Framework Version: {CheckFor45PlusVersion((int)ndpKey.GetValue("Release"))}");
                }
                else
                {
                    Console.WriteLine(".NET Framework Version 4.5 or later is not detected.");
                }
            }

        }

        // Checking the version using >= enables forward compatibility.
        // This example displays output like the following:
        //       .NET Framework Version: 4.6.1
        private static string CheckFor45PlusVersion(int releaseKey)
        {

            //releaseKey = 0;

            if (releaseKey > 461814)
                return "4.7.2 or later";
            if (releaseKey >= 461808)
                return "4.7.2";
            if (releaseKey >= 461308)
                return "4.7.1";
            if (releaseKey >= 460798)
                return "4.7";
            if (releaseKey >= 394802)
                return "4.6.2";
            if (releaseKey >= 394254)
                return "4.6.1";
            if (releaseKey >= 393295)
                return "4.6";
            if (releaseKey >= 379893)
                return "4.5.2";
            if (releaseKey >= 378675)
                return "4.5.1";
            if (releaseKey >= 378389)
                return "4.5";
            // This code should never execute. A non-null release key should mean
            // that 4.5 or later is installed.
            //return "No 4.5 or later version detected";


            if (MessageBox.Show("No 4.5 or later version detected", "Exit", MessageBoxButton.OK, MessageBoxImage.Warning) ==
                MessageBoxResult.OK)
            {
                //Application.Current.Shutdown();
                Environment.Exit(0);

            }

            return "No 4.5 or later version detected";

        }
        void App_DispatcherUnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            logger.Log(LogLevel.Fatal, e.Exception, "An unexpected application exception occurred");
            // Process unhandled exception
            MessageBox.Show("An unexpected exception has occurred. Shutting down the application. Please check the log file for more details.");
            // Prevent default unhandled exception processing
            e.Handled = true;
            Environment.Exit(0);
        }

        //private void Application_Exit(object sender, ExitEventArgs e)
        //{
        //    e.ApplicationExitCode = 99;
        //}
    }
}



