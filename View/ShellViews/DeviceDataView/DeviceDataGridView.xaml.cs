using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using GalaSoft.MvvmLight.Messaging;
using LauncherMvvmLight.MessageInfrastructure;
using LauncherMvvmLight.Model;
using LauncherMvvmLight.ViewModel;


namespace LauncherMvvmLight.View.ShellViews.DeviceDataView
{
    /// <summary>
    /// Interaction logic for DeviceDataView.xaml
    /// </summary>
    public partial class DeviceDataGridView : UserControl
    {
        //Access the view model
        //protected DeviceDataGridViewModel m_ViewModel
        //{
        //    get { return (DeviceDataGridViewModel)Resources["ViewModel"]; }
        //}

        public DeviceDataGridView()
        {
            var viewModel = (DeviceDataGridViewModel)DataContext;

            InitializeComponent();

            //m_ViewModel.Initialize();
            //Loaded += delegate { m_ViewModel.OnLoaded(); };
            //Unloaded += delegate { m_ViewModel.OnUnloaded(); };
            //viewModel.Initialize();
            //Loaded += delegate { viewModel.OnLoaded(); };
            //Unloaded += delegate { viewModel.OnUnloaded(); };
            //InitStyles();
            //RegisterMessenger();



        }

        protected  void OnNavigatedTo(NavigationEventArgs e)
        {
            Console.WriteLine("DeviceDataGridViewModel->OnNavigatedTo()");
            // TODO: Add your initialization code here 
            // This method is only called when the application is running
        }

        void OnUserControlLoaded(Object sender, RoutedEventArgs e)
        {
            //Messenger.Default.Send<NotificationMessage>(new NotificationMessage("DeviceDataGridLoaded"));

            //dgdev.CommitEdit(DataGridEditingUnit.Row, true);
        }

        void OnUserControlUnloaded(Object sender, RoutedEventArgs e)
        {
            //dgdev.CancelEdit(DataGridEditingUnit.Row);
        }


        #region styles

        private void InitStyles()
        {
            //Style rowStyle = new Style(typeof(DataGridRow));
            // rowStyle.Setters.Add(new EventSetter(DataGridRow.MouseDoubleClickEvent,
            //                          new MouseButtonEventHandler(Row_DoubleClick)));
            //  dgdev.RowStyle = rowStyle;
        }

        #endregion

        #region event_handlers

        private void Row_DoubleClick(object sender, MouseButtonEventArgs e)
        {

            //DataGridRow row = sender as DataGridRow;
            // Some operations with this row
            //m_ViewModel.Row_DoubleClickHandler(row);

        }

        #endregion

    }

}
