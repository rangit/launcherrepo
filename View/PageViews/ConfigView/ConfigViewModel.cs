﻿using System;
using System.Windows.Threading;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using LauncherMvvmLight.MessageInfrastructure;
using LauncherMvvmLight.Model;
using NLog;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using LauncherMvvmLight.Domain.Services.DBService;

namespace LauncherMvvmLight.View.ConfigViews
{
    public class ConfigViewModel : ViewModelBase
    {       

        #region Fields
        private IDBSrv _dbService;
        private MessageReceiver<DeviceSlectedConfigMsgPayload> _deviceSlectedConfigMsgPayloadReceiver;
        private MessageSender<DeviceUpdatedConfigMsgPayload> _deviceUpdatedConfigMsgPayloadSender;

        private IFrameNavigationService _navigationService;
        private bool _isLoaded = false;
        private static string ViewTitle = "Config Data";
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private DispatcherTimer timer = null;
        private DeviceOnlyInfoModel _SelectedDevInfo;
        public List<DeviceTableDbModel> _devicesListPtr { get; private set; }
        public List<DeviceConfigTableModel> _devicesConfigListPtr { get; private set; }
        public String DisplayDetailConfig { get; private set; }
        public RelayCommand<DeviceInfoModel> SaveConfigCommand { get; set; }
        public RelayCommand<DeviceInfoModel> CancelConfigCommand { get; set; }
        public DeviceOnlyInfoModel selectedCard;
        public RelayCommand<string> ConnectionTypeSelection { get; set; }


        #endregion Fields

        #region Properties

        public string IdType { get; set; }


        private String _lblSelectedDevice;
        public String LblSelectedDevice
        {
            get { return _lblSelectedDevice; }
            set
            {
                _lblSelectedDevice = value;

                RaisePropertyChanged("lblSelectedDevice");
            }
        }

        private String _cmbSelectedIdTypes;
        public String CmbSelectedIdTypes
        {
            get { return _cmbSelectedIdTypes; }
            set
            {
                _cmbSelectedIdTypes = value;

                RaisePropertyChanged("cmbIdType");
            }
        }



        private bool _radEthernetIsChecked;
        public bool RadEthernetIsChecked
        {
            get
            {
                return _radEthernetIsChecked;
            }
            set
            {
                _radEthernetIsChecked = value;

                RaisePropertyChanged("RadEthernetIsChecked");
            }
        }

        private bool _isEthUCVisable;
        public bool IsEthUCVisable
        {
            get
            {
                return _isEthUCVisable;
            }
            set
            {
                _isEthUCVisable = value;

                RaisePropertyChanged("IsEthUCVisable");
            }
        }
        private bool _isUsbUCVisable;
        public bool IsUsbUCVisable
        {
            get
            {
                return _isUsbUCVisable;
            }
            set
            {
                _isUsbUCVisable = value;

                RaisePropertyChanged("IsUsbUCVisable");
            }
        }
        /*
        private bool _radEthernetIsChecked;
        public bool radEthernetIsChecked
        {
            get
            {
                return radEthernetIsChecked;
            }
            set
            {
                _radEthernetIsChecked = value;

                RaisePropertyChanged("radEthernet1");
                RaisePropertyChanged("txtBoxIPAddress");
            }
        }
        */
        /*
                private bool _radEthernet1_Checked;
                public bool radEthernet1_Checked
                {
                    get
                    {
                        return radEthernet1_Checked;
                    }
                    set
                    {
                        _radEthernet1_Checked = value;

                        RaisePropertyChanged("radEthernet1");
                        RaisePropertyChanged("txtBoxIPAddress");
                    }
                }
        */


        private bool _radUsbIsChecked;
        public bool RadUsbIsChecked
        {
            get
            {
                return _radUsbIsChecked;
            }
            set
            {
                _radUsbIsChecked = value;

                RaisePropertyChanged("RadUsbIsChecked");
            }
        }



        private string _radConnTypeChecked;
        public string RadConnTypeChecked
        {
            get
            {
                return _radConnTypeChecked;
            }
            set
            {
                _radConnTypeChecked = value;

                if (value == "Ethernet") //handle ethernet selection
                {
                    IsEthUCVisable = true;
                    IsUsbUCVisable = false;
                    RaisePropertyChanged("RadConnTypeChecked");
                }
                else if (value == "USB") //handle usb selection
                {
                    IsEthUCVisable = false;
                    IsUsbUCVisable = true;

                    RaisePropertyChanged("RadConnTypeChecked");
                }


                //RaisePropertyChanged("RadConnTypeChecked");
            }
        }



        /*
                private bool _radUSB1_Checked;
                public bool radUSB1_Checked
                {
                    get
                    {
                        return radUSB1_Checked;
                    }
                    set
                    {
                        _radUSB1_Checked = value;

                        RaisePropertyChanged("radUSB1");
                        RaisePropertyChanged("txtSerialNumber");
                    }
                }
        */

        private String _ipADDCommand;
        public String ipADDCommand
        {
            get { return _ipADDCommand; }
            set
            {
                _ipADDCommand = value;

                RaisePropertyChanged("lbIPAddress");
            }
        }
        private String _usbADDCommand;
        public String UsbADDCommand
        {
            get { return _usbADDCommand; }
            set
            {
                _usbADDCommand = value;

                RaisePropertyChanged("TxtBoxUsbAddress");
            }
        }

        private DeviceInfoModel _lstCardsSelectedItemInfoModel;
        public DeviceInfoModel LstCardsSelectedItemInfoModel
        {
            get { return _lstCardsSelectedItemInfoModel; }
            set
            {
                if (value == _lstCardsSelectedItemInfoModel)
                    return;

                _lstCardsSelectedItemInfoModel = value;

                //RaisePropertyChanged("LstCardsSelectedItemInfoModel");

                //LblSelectedDevice = "Device " + selectedCard.dev + " :: " + _lstCardsSelectedItem.DeviceTypeName;

                // selection changed - do Handler
                UpdateConfigPageDynamicDisplay(_lstCardsSelectedItem.DeviceTypeName,
                                            _lstCardsSelectedItem.DeviceTypeDisplayConfig,
                                            _lstCardsSelectedItem.DipConfig,
                                            selectedCard.IPAdd,
                                            selectedCard.SlotID,
                                            selectedCard.USBSN,
                                            selectedCard.ConnectType);

            }
        }






        private DeviceConfigTableModel _lstCardsSelectedItem;
        public DeviceConfigTableModel LstCardsSelectedItem
        {
            get { return _lstCardsSelectedItem; }
            set
            {
                //if (value == _lstCardsSelectedItem)
                //    return;

                _lstCardsSelectedItem = value;

                RaisePropertyChanged("LstCardsSelectedItem");

                LblSelectedDevice = "Device " + _SelectedDevInfo.dev + " :: " + _lstCardsSelectedItem.DeviceTypeName; // Was: selectedCard

                // selection changed - do Handler
                UpdateConfigPageDynamicDisplay(_lstCardsSelectedItem.DeviceTypeName,
                    _lstCardsSelectedItem.DeviceTypeDisplayConfig,
                    _lstCardsSelectedItem.DipConfig,
                    _SelectedDevInfo.IPAdd, //IPAdd selectedCard.IPAdd,
                    _SelectedDevInfo.SlotID, //selectedCard.SlotID,
                    _SelectedDevInfo.USBSN, //selectedCard.USBSN,
                    _SelectedDevInfo.ConnectType); //selectedCard.ConnectType);


            }
        }




        private ObservableCollection<DeviceTableDbModel> _devicesListDB;

        public ObservableCollection<DeviceTableDbModel> DevicesListDB
        {
            get
            {
                return _devicesListDB;
            }
            set
            {
                _devicesListDB = value;
                RaisePropertyChanged("DevicesListDB");
            }
        }

        private ObservableCollection<DeviceConfigTableModel> _devicesConfigListDB;
        private object groupLstCards;
        private object lstCards;

        public ObservableCollection<DeviceConfigTableModel> DevicesConfigListDB
        {
            get
            {
                return _devicesConfigListDB;
            }
            set
            {
                _devicesConfigListDB = value;
                RaisePropertyChanged("DevicesConfigListDB");
            }
        }

        public RelayCommand SelectionChangedLbx { get; private set; }
        void SelectionChangedLbxHandler()
        {

        }

        public DeviceOnlyInfoModel SelectedDevInfo
        {
            get { return _SelectedDevInfo; }
            set
            {
                _SelectedDevInfo = value;
                RaisePropertyChanged("SelectedDevInfo");
            }
        }


        private ObservableCollection<DeviceOnlyInfoModel> _selectedDeviceInfo;
        public ObservableCollection<DeviceOnlyInfoModel> selectedDeviceInfo
        {
            get
            {
                return _selectedDeviceInfo;
            }
            set
            {
                _selectedDeviceInfo = value;
                RaisePropertyChanged("selectedDeviceInfo");
            }
        }
        #endregion Properties

        #region Initialization
        public ConfigViewModel(IFrameNavigationService navigationService, IDBSrv dbService)
        {

            DisplayDetailConfig = "";
            SelectionChangedLbx = new RelayCommand(SelectionChangedLbxHandler);
            IsUsbUCVisable = false;
            IsEthUCVisable = false;

            _navigationService = navigationService;
            _dbService = dbService;
            _devicesListPtr = _dbService.GetDeviceTableData();
            _devicesConfigListPtr = _dbService.GetConfigTypeList();
            DevicesListDB = new ObservableCollection<DeviceTableDbModel>(_devicesListPtr);
            DevicesConfigListDB = new ObservableCollection<DeviceConfigTableModel>(_devicesConfigListPtr);

            ConnectionTypeSelection = new RelayCommand<string>(ConnectionTypeSelectionHandler);


            RegisterMessenger();
            SaveConfigCommand = new RelayCommand<DeviceInfoModel>(_SaveConfig);
            CancelConfigCommand = new RelayCommand<DeviceInfoModel>(_CancelConfig);

            DeviceOnlyInfoModel SelectedCard = (DeviceOnlyInfoModel)navigationService.Parameter;
            selectedCard = (DeviceOnlyInfoModel)navigationService.Parameter;

            /*
              for (int i = 0; i < DevicesConfigListDB.Count; i++)
              {
                  if ( (DevicesConfigListDB[i].DeviceTypeName == SelectedCard.strCardType) || (DevicesConfigListDB[i].DeviceTypeName == SelectedCard.ExcConfigDeviceType)||
                       ((DevicesConfigListDB[i].DeviceTypeName == "Undefined" )&&(SelectedCard.strCardType == "")))
                  {
                      LstCardsSelectedItem = DevicesConfigListDB[i];
                      CmbSelectedIdTypes = SelectedCard.SlotID;

                      break;
                  }
              }
              */

        }


        
        void ConnectionTypeSelectionHandler(string selection)
        {
            int i = 0;
        }

        void _SaveConfig(DeviceInfoModel dev)
        {
            //_navigationService.NavigateTo("Shell");

            //selectedCard.strCardType = LstCardsSelectedItem.DeviceTypeName;
            _SelectedDevInfo.strCardType = LstCardsSelectedItem.DeviceTypeName;

            //_deviceUpdatedConfigMsgPayloadSender.SendMessage(new DeviceUpdatedConfigMsgPayload(_SelectedDevInfo));

            //Messenger.Default.Send<DeviceSlectedConfigMsg>(new DeviceSlectedConfigMsg(selectedCard));
            _navigationService.GoBack();
        }

        void _CancelConfig(DeviceInfoModel dev)
        {
            //_navigationService.NavigateTo("Shell");
            _navigationService.GoBack();
        }


        private void RegisterMessenger()
        {
            //initialize sendesrs
            _deviceUpdatedConfigMsgPayloadSender = new MessageSender<DeviceUpdatedConfigMsgPayload>();

            Messenger.Default.Register<UpdateConnectionTypeMessage>(this, msg =>
            {
                if (msg != null)
                    RadConnTypeChecked = msg.ConnType;             
            });

            //initialize receivers

            _deviceSlectedConfigMsgPayloadReceiver = new MessageReceiver<DeviceSlectedConfigMsgPayload>((message) =>
            {
                _SelectedDevInfo = message.Payload;

                for (int i = 0; i < DevicesConfigListDB.Count; i++)
                {
                    //if ((DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.strCardType) || (DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.ExcConfigDeviceType))


                    if ((DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.strCardType) || (DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.ExcConfigDeviceType) ||
                                ((DevicesConfigListDB[i].DeviceTypeName == "Undefined") && (this.SelectedDevInfo.strCardType == "")))


                    {
                        LstCardsSelectedItem = DevicesConfigListDB[i];
                        CmbSelectedIdTypes = this.SelectedDevInfo.SlotID;

                        break;
                    }
                }

            }, true);

            //Send out a message to see if another VM has params we need
            /* Was:
            Messenger.Default.Send<DeviceSlectedConfigMsg>(new DeviceSlectedConfigMsg(deviceInfo =>
            {
                if (deviceInfo != null)
                {
                    this.SelectedDevInfo = deviceInfo;

                }
            }));
            */
            //Register any message pipes
            /* Was:
            Messenger.Default.Register<DeviceSlectedConfigMsg>(this, msg =>
            {
                if(msg != null)
                    this.SelectedDevInfo = msg.Payload;

               
                
                for (int i = 0; i < DevicesConfigListDB.Count; i++)
                {
                       //if ((DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.strCardType) || (DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.ExcConfigDeviceType))


                        if ((DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.strCardType) || (DevicesConfigListDB[i].DeviceTypeName == this.SelectedDevInfo.ExcConfigDeviceType) ||
                                    ((DevicesConfigListDB[i].DeviceTypeName == "Undefined") && (this.SelectedDevInfo.strCardType == "")))


                        {
                            LstCardsSelectedItem = DevicesConfigListDB[i];
                            CmbSelectedIdTypes = this.SelectedDevInfo.SlotID;

                        break;
                        }
                }
                

            });
            */
        }
        #endregion Initialization

        #region Methods        
        public void UpdateConfigPageDynamicDisplay(string deviceDisplayName, string deviceDisplayConfig,
                                                   string dipConfig, string ipADD, string SlotID, string USBSN, string ConnectType)
        {

            DisplayDetailConfig = deviceDisplayName + "_" + deviceDisplayConfig + "_" + dipConfig + "_" + ipADD + "_" + SlotID + "_" + USBSN + "_" + ConnectType;


            MessengerInstance.Send<NotificationMessage>(new NotificationMessage(DisplayDetailConfig));

        }

        public void CallUpdateConfigPageDynamicDisplay()
        {
            // Some code
        }
        public void Initialize()
        {
            // TODO: Add your initialization code here 
            // This method is only called when the application is running
        }

        public void OnLoaded()
        {
            if (!_isLoaded)
            {
                // TODO: Add your loaded code here 
                _isLoaded = true;
            }
        }

        public void OnUnloaded()
        {
            if (_isLoaded)
            {
                // TODO: Add your cleanup/unloaded code here 
                _isLoaded = false;
            }
        }
        #endregion Methods

    }


}