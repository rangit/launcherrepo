﻿using System.Windows.Controls;
using System;
using System.Windows;
using GalaSoft.MvvmLight.Messaging;
using LauncherMvvmLight.View.ConfigViews;
using LauncherMvvmLight.MessageInfrastructure;



namespace LauncherMvvmLight.View.PageViews.ConfigView
{
    /// <summary>
    /// Interaction logic for HelpView.xaml
    /// </summary>
    public partial class Config : Page
    {
        private object navigationService;

        public Config()
        {
            InitializeComponent();

            RegisterMessenger();


        }
        private void RegisterMessenger()
        {
            //Register any message pipes
            Messenger.Default.Register<NotificationMessage>(this, NotifyMe);

        }

        public void NotifyMe(NotificationMessage notificationMessage)
        {
            String notificationString = notificationMessage.Notification;
            String[] notificationParams = notificationString.Split('_');

            string DeviceTypeName = notificationParams[0];
            string DeviceTypeDisplayConfig = notificationParams[1];
            string DipConfig = notificationParams[2];
            string ipADD = notificationParams[3];
            string SlotID = notificationParams[4];
            string USBSN = notificationParams[5];
            string ConnectType = notificationParams[6];


            //  radEthernet1.IsChecked = true;
            //           radUSB1.IsChecked = false;
            //txtBoxIPAddress.Text = ipADD;
            //txtSerialNumber.Text = USBSN;


            if (DeviceTypeDisplayConfig == "custom2")
            {

                GBoxIdentifier.Visibility = Visibility.Collapsed;
                GBoxConnType.Visibility = Visibility.Collapsed;
                GBoxDbgDetail.Visibility = Visibility.Visible;
                GBoxEthernetConfig.Visibility = Visibility.Visible;
                GroupBoxUsbConfig.Visibility = Visibility.Collapsed;
                RBtnEthernet.IsChecked = true;
               
                if (ConnectType == "NET")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Visible;
                    GroupBoxUsbConfig.Visibility = Visibility.Collapsed;
                    RBtnEthernet.IsChecked = true;
                    if (ipADD == "")
                        TBoxIpAddress.Text = "010.072.063.045";
                    else
                        TBoxIpAddress.Text = ipADD;
                }

                CBoxDbgDetail0.Content = "1553 Module 0";
                CBoxDbgDetail0.Visibility = Visibility.Visible;
                CBoxDbgDetail1.Content = "Discrete Module";
                CBoxDbgDetail1.Visibility = Visibility.Visible;
                CBoxDbgDetail2.Content = "";
                CBoxDbgDetail2.Visibility = Visibility.Collapsed;
                CBoxDbgDetail3.Content = "";
                CBoxDbgDetail3.Visibility = Visibility.Collapsed;

              

            }
            else if (DeviceTypeDisplayConfig == "custom3")
            {
 
                GBoxIdentifier.Visibility = Visibility.Collapsed;
                GBoxConnType.Visibility = Visibility.Visible;
                GBoxDbgDetail.Visibility = Visibility.Visible;


                RBtnEthernet.IsChecked = true;

                if (ConnectType == "USB")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Collapsed;
                    GroupBoxUsbConfig.Visibility = Visibility.Visible;
                    RBtnUsb.IsChecked = true;
                    TBlkUsbInfo.Text =
                        "If you are using only a single USB device,you mayset this field to * to indicate to use any USB irrespective to Serial Number. " + 
                        "However, if you are using multiple USB devices, use this field to specify the Serial Number";


                    if (USBSN == "")
                        TxtBoxUsbAddress.Text = "*____";
                    else
                        TxtBoxUsbAddress.Text = USBSN;

                }
                else //if (ConnectType == "NET")
                {                    
                    if(ipADD == "")
                        TBoxIpAddress.Text = "010.072.063.045";
                    else
                        TBoxIpAddress.Text = ipADD;
                }

                CBoxDbgDetail0.Content = "429 Module";
                CBoxDbgDetail0.Visibility = Visibility.Visible;
                CBoxDbgDetail1.Content = "Discrete Module";
                CBoxDbgDetail1.Visibility = Visibility.Visible;
                CBoxDbgDetail2.Content = "";
                CBoxDbgDetail2.Visibility = Visibility.Collapsed;
                CBoxDbgDetail3.Content = "";
                CBoxDbgDetail3.Visibility = Visibility.Collapsed;


            }
            else if (DeviceTypeDisplayConfig == "custom4") //unet 1553
            {
                GBoxIdentifier.Visibility = Visibility.Collapsed;
                GBoxConnType.Visibility = Visibility.Visible;
                GBoxDbgDetail.Visibility = Visibility.Visible;

                if (ConnectType == "USB")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Collapsed;
                    GroupBoxUsbConfig.Visibility = Visibility.Visible;
                    RBtnUsb.IsChecked = true;
                    TBlkUsbInfo.Text =
                        "If you are using only a single USB device,you mayset this field to * to indicate to use any USB irrespective to Serial Number. " +
                        "However, if you are using multiple USB devices, use this field to specify the Serial Number";

                    if (USBSN == "")
                        TxtBoxUsbAddress.Text = "*____";
                    else
                        TxtBoxUsbAddress.Text = USBSN;

                }
                else //if (ConnectType == "NET")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Visible;
                    GroupBoxUsbConfig.Visibility = Visibility.Collapsed;
                    RBtnEthernet.IsChecked = true;

                    if (ipADD == "")
                        TBoxIpAddress.Text = "010.072.063.045";
                    else
                        TBoxIpAddress.Text = ipADD;
                
                }

                CBoxDbgDetail0.Content = "1553 Module 0";
                CBoxDbgDetail0.Visibility = Visibility.Visible;
                CBoxDbgDetail1.Content = "1553 Module 1";
                CBoxDbgDetail1.Visibility = Visibility.Visible;
                CBoxDbgDetail2.Content = "Discrete Module";
                CBoxDbgDetail2.Visibility = Visibility.Visible;
                CBoxDbgDetail3.Content = "";
                CBoxDbgDetail3.Visibility = Visibility.Collapsed;


            }
            else if (DeviceTypeDisplayConfig == "custom5")
            {
                GBoxIdentifier.Visibility = Visibility.Collapsed;
                GBoxConnType.Visibility = Visibility.Visible;
                GBoxDbgDetail.Visibility = Visibility.Visible;

                if (ConnectType == "USB")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Collapsed;
                    GroupBoxUsbConfig.Visibility = Visibility.Visible;
                    RBtnUsb.IsChecked = true;
                    TBlkUsbInfo.Text =
                        "If you are using only a single USB device,you mayset this field to * to indicate to use any USB irrespective to Serial Number. " +
                        "However, if you are using multiple USB devices, use this field to specify the Serial Number";

                    if (USBSN == "")
                        TxtBoxUsbAddress.Text = "*____";
                    else
                        TxtBoxUsbAddress.Text = USBSN;

                }
                else //if (ConnectType == "NET")
                {
                    GBoxEthernetConfig.Visibility = Visibility.Visible;
                    GroupBoxUsbConfig.Visibility = Visibility.Collapsed;
                    RBtnEthernet.IsChecked = true;

                    if (ipADD == "")
                        TBoxIpAddress.Text = "010.072.063.045";
                    else
                        TBoxIpAddress.Text = ipADD;

                }

                CBoxDbgDetail0.Content = "MACC Module 0";
                CBoxDbgDetail0.Visibility = Visibility.Visible;
                CBoxDbgDetail1.Content = "MACC Module 1";
                CBoxDbgDetail1.Visibility = Visibility.Visible;
                CBoxDbgDetail2.Content = "MACC Module 2";
                CBoxDbgDetail2.Visibility = Visibility.Visible;
                CBoxDbgDetail3.Content = "MACC Module 3";
                CBoxDbgDetail3.Visibility = Visibility.Visible;



            }            
            else //all other configurations
            {
                GBoxIdentifier.Visibility = Visibility.Visible;
                GBoxConnType.Visibility = Visibility.Collapsed;
                GBoxDbgDetail.Visibility = Visibility.Collapsed;
                GBoxEthernetConfig.Visibility = Visibility.Collapsed;
                GroupBoxUsbConfig.Visibility = Visibility.Collapsed;


               
                if (DeviceTypeDisplayConfig == "custom0")
                {
                    TBlkUniqIdInfo.Text = "";
                    LblUniqueIdInfo.Content = "";
                    CBoxIdType.Items.Clear();
                    CBoxIdType.Items.Add("N/A");
                    CBoxIdType.SelectedIndex = 0;
                    CBoxIdType.IsEnabled = false;
                }
                else if (DeviceTypeDisplayConfig == "custom1")
                {
                    TBlkUniqIdInfo.Text =
                        "If you are using a single card, you may set this field to auto.  However, if you are using multiple cards, you must set a unique ID for each one, using dip switch SW1 on the card.";
                    LblUniqueIdInfo.Content = "Unique ID: ";
                    CBoxIdType.IsEnabled = true;

                    int DipNum = Int16.Parse(DipConfig);
                    if (CBoxIdType.Items.Count != DipNum)
                    {
                        CBoxIdType.Items.Clear();                       
                        CBoxIdType.Items.Add("Auto");
                        for (int id = 0; id < (DipNum - 1); id++)
                            CBoxIdType.Items.Add(id.ToString());

                        if ((SlotID != "") && (SlotID != "65535") && (SlotID != "N/A"))
                            CBoxIdType.SelectedIndex = Convert.ToInt32(SlotID) + 1;
                        else
                            CBoxIdType.SelectedIndex = 0;
                          
                    }
                }


            }
        }

        private void UniqueIdentifierUCView_Loaded(object sender, RoutedEventArgs e)
        {

           var viewModel = (ConfigViewModel)DataContext;
           viewModel.CmbSelectedIdTypes = viewModel.selectedCard.SlotID;
           viewModel.UpdateConfigPageDynamicDisplay(viewModel.LstCardsSelectedItem.DeviceTypeName, viewModel.LstCardsSelectedItem.DeviceTypeDisplayConfig, 
                                                     viewModel.LstCardsSelectedItem.DipConfig, viewModel.selectedCard.IPAdd,  viewModel.selectedCard.SlotID, viewModel.selectedCard.USBSN, viewModel.selectedCard.ConnectType);
              
            

           // viewModel.ipADDCommand = viewModel.selectedCard.IPAdd;
            

     /*       if (viewModel.selectedCard.ConnectType == "USB")
            {
                viewModel.radUSB1_Checked = true;
                
            }
            else if (viewModel.selectedCard.ConnectType == "NET")
            {
                viewModel.radEthernet1_Checked = true;
            }
            

            //viewModel.rtad
            */

       }


        private void RBtn_ConnType_Checked(object sender, RoutedEventArgs e)
        {
            // ... Get RadioButton reference.
            var button = sender as RadioButton;

            // ... Display button content as title.
            String Title = button.Content.ToString();


            if (Title == "USB")
            {
                GBoxEthernetConfig.Visibility = Visibility.Collapsed;
                GroupBoxUsbConfig.Visibility = Visibility.Visible;
                RBtnUsb.IsChecked = true;
                TBlkUsbInfo.Text =
                    "If you are using only a single USB device,you mayset this field to * to indicate to use any USB irrespective to Serial Number. " +
                    "However, if you are using multiple USB devices, use this field to specify the Serial Number";

               
               TxtBoxUsbAddress.Text = "*____";


            }
            else //if (ConnectType == "NET")
            {
                GBoxEthernetConfig.Visibility = Visibility.Visible;
                GroupBoxUsbConfig.Visibility = Visibility.Collapsed;
                RBtnEthernet.IsChecked = true;
                TBoxIpAddress.Text = "010.072.063.045";
               
            }


            /*
            Messenger.Default.Send<UpdateConnectionTypeMessage>(new UpdateConnectionTypeMessage()
            {
                ConnType = Title
            });
            */

        }


        private void UniqueIdentifierUI_Loaded(object sender, RoutedEventArgs e)
        {


            //GBoxIdentifier.Visibility = Visibility.Collapsed;
            GBoxConnType.Visibility = Visibility.Collapsed;
            GBoxDbgDetail.Visibility = Visibility.Collapsed;
            GBoxEthernetConfig.Visibility = Visibility.Collapsed;
            GroupBoxUsbConfig.Visibility = Visibility.Collapsed;

            var viewModel = (ConfigViewModel) DataContext;
            viewModel.CmbSelectedIdTypes = viewModel.selectedCard.SlotID;
            viewModel.UpdateConfigPageDynamicDisplay(viewModel.LstCardsSelectedItem.DeviceTypeName,
                viewModel.LstCardsSelectedItem.DeviceTypeDisplayConfig,
                viewModel.LstCardsSelectedItem.DipConfig, viewModel.selectedCard.IPAdd, viewModel.selectedCard.SlotID,
                viewModel.selectedCard.USBSN, viewModel.selectedCard.ConnectType);


        }

        private void BtnSerialNum_Clicked(object sender, RoutedEventArgs e)
        {
            TxtBoxUsbAddress.Text = "*____";
        }
    }
}
