﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GalaSoft.MvvmLight.Messaging;
using LauncherMvvmLight.MessageInfrastructure;
using LauncherMvvmLight.View.ConfigViews;
using LauncherMvvmLight.ViewModel;

namespace LauncherMvvmLight.Design.UserControls
{
    /// <summary>
    /// Interaction logic for ConnectionConfigUCView.xaml
    /// </summary>
    public partial class LedLightUCView : UserControl
    {
        public LedLightUCView()
        {
            InitializeComponent();
            //myViewModel = (ConfigViewModel)DataContext;
            //this.DataContext = App.Current.Resources["Locator"] as ViewModelLocator).ConfigViewModel;
            //Register any message pipes
            RegisterMessenger();
        }

        private void RegisterMessenger()
        {
            //Register any message pipes
            //Messenger.Default.Register<NotificationMessage>(this, NotifyMe);

        }


    }
}
