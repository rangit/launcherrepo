﻿using GalaSoft.MvvmLight.Messaging;
using LauncherMvvmLight.Model;

namespace LauncherMvvmLight.MessageInfrastructure
{
    public abstract class InitMessageBase : MessageBase
    {
        // Used for a send
        public bool Received { get; set; }

        public InitMessageBase()
        {

        }
    }
    public class MessageCommunicator
    {
        public DeviceInfoModel Dev { get; set; }
    }


    public class DeviceSlectedConfigMsgPayload : InitMessageBase
    {
        public DeviceOnlyInfoModel Payload { get; set; }


        public DeviceSlectedConfigMsgPayload()
        {
          
        }

        public DeviceSlectedConfigMsgPayload(DeviceOnlyInfoModel payload)
        {
            this.Payload = payload;
        }

    }


    public class DeviceUpdatedConfigMsgPayload : InitMessageBase
    {
        public DeviceOnlyInfoModel Payload { get; set; }


        public DeviceUpdatedConfigMsgPayload()
        {

        }

        public DeviceUpdatedConfigMsgPayload(DeviceOnlyInfoModel payload)
        {
            this.Payload = payload;
        }

    }

    public class UpdateConnectionTypeMessage
    {
        public string ConnType { get; set; }
    }

}
