﻿using System;

namespace LauncherMvvmLight.MessageInfrastructure
{
    public class UpdateTimeMessage
    {
        public DateTime dateTime { get; set; }
    }
}
