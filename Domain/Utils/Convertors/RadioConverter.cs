﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace LauncherMvvmLight.Domain.Utils.Convertors
{
    public enum ConnectionTypes
    {
        EthernetType,
        UsbType,
        Undefined
    }
    public class ConnectionTypesConvert : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int p = 2;
            int.TryParse((string)parameter, out p);
            if (value is ConnectionTypes)
            {
                ConnectionTypes v = (ConnectionTypes)value;
                if (v == ConnectionTypes.EthernetType && p == 0)
                    return true;
                if (v == ConnectionTypes.UsbType && p == 1)
                    return true;           
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int p = 2;
            int.TryParse((string)parameter, out p);
            if (value is bool && (bool)value)
                switch (p)
                {
                    case 0:
                        return ConnectionTypes.EthernetType;
                    case 1:
                        return ConnectionTypes.UsbType;
                    case 2:
                        return ConnectionTypes.Undefined;
                }
            return null;
        }
    }

    public class RadioConvertor: IValueConverter {

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
           return value.Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture) {
            return System.Convert.ToBoolean(value)? parameter:null;
        }
    }
}