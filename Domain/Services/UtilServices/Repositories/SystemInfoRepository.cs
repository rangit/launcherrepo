﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using LauncherMvvmLight.Domain.Utils;
using LauncherMvvmLight.Model;


namespace LauncherMvvmLight.Domain.Services.UtilServices
{
    /// <summary>
    /// This class manages a note repository.
    /// The devs will be saved using serialization.
    /// </summary>
    public class SystemInfoRepository : ISystemInfoRepository
    {
        private List<SystemInfoModel> _dataStore;
        private readonly string _dataFile;
        private readonly string _dataFileType;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceInfoRepository"/> class.
        /// </summary>
        /// <param name="fileName">The file name.</param>
        public SystemInfoRepository(string fileName , string fileType)
        {
            _dataStore = new List<SystemInfoModel>();
            if ((fileName != null) && (fileType != null))
            {
                if ((fileType == "xml") || (fileType == "bin") || (fileType == "json"))
                {
                    if (fileName != "")
                    {
                        fileType = string.Concat("." + fileType);
                        _dataFileType = fileType;
                        fileName = string.Concat(fileName + fileType);

                        //_dataFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName);
                        _dataFile = Path.Combine(@"C:\Excalibur\Reports", fileName);

                        //Deserialize();
                    }
                }
            }
        }

        /// <summary>
        /// Saves the specified note.
        /// </summary>
        /// <param name="dev">The note.</param>
        public void Save(SystemInfoModel data)
        {
            if (!_dataStore.Contains(data))
                _dataStore.Add(data);

            //Serialize();

            if (_dataFileType == ".xml")
                SerilaizeUtil.WriteToXmlFile<SystemInfoModel>(_dataFile, data);
        }

        /// <summary>
        /// Deletes the specified note.
        /// </summary>
        /// <param name="note">The note.</param>
        public void Delete(SystemInfoModel data)
        {
            _dataStore.Remove(data);

            //Serialize();
            if (_dataFileType == ".xml")
                SerilaizeUtil.WriteToXmlFile<SystemInfoModel>(_dataFile, data);
        }

        /// <summary>
        /// Resets the repository.
        /// </summary>
        public void RepositoryReset()
        {
            File.Delete(_dataFile);
            _dataStore = new List<SystemInfoModel>();
        }

        /// <summary>
        /// Returns the entire repository.
        /// </summary>
        /// <returns>A List with all Devs</returns>
        public List<SystemInfoModel> GetAllData()
        {
            return _dataStore;
        }
        public List<OsDataModel> GetOsData()
        {
            return _dataStore[0].osDataModel;
        }
        public List<DeviceManagerDataModel> GetDeviceData()
        {
            return _dataStore[0].DeviceDriverDataList;
        }
        public List<ModuleDataModel> GetModuleData()
        {
            //return _dataStore[0].ModuleDataList;
            var myRegex = new Regex(@"^Exc$");
            var results = from myobject in _dataStore[0].ModuleDataList
                          where(myRegex.IsMatch(myobject.Name))
                          select myobject;

            return results.ToList();

        }

        /// <summary>
        /// Serializes all the notes to a file.
        /// </summary>
        private void Serialize()
        {            

            using (var stream = File.Open(_dataFile, FileMode.OpenOrCreate))
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, _dataStore);
               

                //BinaryFormatter formatter = new BinaryFormatter();
                //Stream stream = new FileStream(@"C:\Excalibur\Reports\report.txt", FileMode.Create, FileAccess.Write);
                //formatter.Serialize(stream, _dataStore);
                //stream.Close();
            }

            //MemoryStream m = new MemoryStream();
            //formatter.Serialize(m, _dataStore);
            //string f = Convert.ToBase64String(m.ToArray());
        }

        /// <summary>
        /// Deserializes all notes or creates an empty List.
        /// </summary>
        private void Deserialize()
        {
            if (File.Exists(_dataFile))
                using (var stream = File.Open(_dataFile, FileMode.Open))
                {
                    var formatter = new BinaryFormatter();
                    _dataStore = (List<SystemInfoModel>) formatter.Deserialize(stream);
                }
            else
                _dataStore = new List<SystemInfoModel>();
        }
    }
}
