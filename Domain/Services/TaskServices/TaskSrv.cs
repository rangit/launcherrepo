﻿using System;
using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using System.Threading.Tasks;


namespace LauncherMvvmLight.Domain.Services.TaskService
{
    /// <summary>
    /// The Interface defining methods for Create Employee and Read All Employees  
    /// </summary>
    public interface ITaskSrv
    {
        int RunSerivceTest(string testID);
        int RunSerivceTask(string taskType, int actionParam);
        void StopSerivceTest(string testID);


    }
    /// <summary>
    /// Class implementing IDataAccessService interface and implementing
    /// its methods by making call to the Entities using CompanyEntities object
    /// </summary>
    public class TaskSrv : ITaskSrv
    {
        // First create a runspace
        // You really only need to do this once. Each pipeline you create can run in this runspace.
        private BackgroundWorker _bgWorkerRx;


        public TaskSrv()
        {

        }

       

        public int RunSerivceTest(string testID)
        {

            switch(testID)
            {
                case "Task1":
                    Console.WriteLine("Test= " + testID);
                    break;
                case "Task2":
                    Console.WriteLine("Test= " + testID);
                    break;

                case "Task3":
                    Console.WriteLine("Test= " + testID);
                    break;

                case "Task4":
                    Console.WriteLine("Test= " + testID);
                    break;

                case "Task5":
                    Console.WriteLine("Test= " + testID);
                    break;
                case "StopT":
                    StopSerivceTest(testID);
                    Console.WriteLine("Stop= " + testID);
                    break;
                default:
                    Console.WriteLine("Test unknown= " + testID);
                    break;

            }

            return 0;
        }

        public void StopSerivceTest(string testID)
        {
            throw new NotImplementedException();
        }




        private void Init()
        {
            // Summary:

            // 1. Two ways of using tasks
            //    Task.Factory.StartNew() creates and starts a Task
            //    new Task(() => { ... }) creates a task; use Start() to fire it
            // 2. Tasks take an optional 'object' argument
            //    Task.Factory.StartNew(x => { foo(x) }, arg);
            // 3. To return values, use Task<T> instead of Task
            //    To get the return value. use t.Result (this waits until task is complete)
            // 4. Use Task.CurrentId to identify individual tasks.
        }



        public int RunSerivceTask(string taskType, String action,int actionParam)
        {


            //*** global mutex ****
            /*
            const string appName = "MyApp";
            Mutex gMutex;
            try
            {
                gMutex = Mutex.OpenExisting(appName);
                //got mutex -- program already running
            }
            catch(WaitHandleCannotBeOpenedException e)
            {
                gMutex = new Mutex(false,appName); 
            }
            gMutex.ReleaseMutex();
            */

            switch (taskType)
            {
                case "taskWorker":
                    if (action == "start")
                    {
                        Console.WriteLine("start -> TaskType= " + taskType);
                        InitTaskWorker(actionParam);

                    }
                    else
                    {
                        Console.WriteLine("stop -> TaskType= " + taskType);
                        _bgWorkerRx.CancelAsync();
                    }
                    break;
                case "BGWorker":
                    if (action == "start")
                    {
                        Console.WriteLine("start -> TaskType= " + taskType);
                        InitBGWorker(actionParam);

                    }
                    else
                    {
                        Console.WriteLine("stop -> TaskType= " + taskType);
                        _bgWorkerRx.CancelAsync();
                    }
                    break;
              
                default:
                    Console.WriteLine("Test unknown= " + taskType);
                    break;

            }

            return 0;
        }

        //**************************************************************************************************************
        //  task worker handling        
        //**************************************************************************************************************
        private void InitTaskWorker(int taskWorkerParam)
        {          
            //ConcurrentBag<>
            BlockingCollection<int> messages = new BlockingCollection<int>(new ConcurrentBag<int>(),10);



            int i;
            CancellationTokenSource cts = new CancellationTokenSource();
            //var token = cts.Token;

            Mutex mutex = new Mutex();
            
            //token.Register(() =>
            //{
            //    Console.WriteLine("Cancelation has been requested.");
            //});

            //******** Execute **********
            var taskWorker = new Task<int>((objParam) =>
            {
                while (true)
                {

                    if (cts.IsCancellationRequested) // 1. Soft exit
                                                       // RanToCompletion
                    {
                        break;
                    }
                    else
                    {
                        bool haveLock = mutex.WaitOne();
                        try
                        {
                            messages.Add(1);
                            Thread.Sleep(1000);//1 sec

                            foreach (var itme in messages.GetConsumingEnumerable())
                            {
                                
                            }
                        }
                        finally
                        {
                            if(haveLock) mutex.ReleaseMutex();                            
                        }                       
                    }
                }

                return 1;
            }
            //,taskWorker_DoWork(objParam, token)}
            //, taskWorkerParam, token);
            ,taskWorkerParam);
            taskWorker.Start();

            
            
            
            //******** Wait for finish **********
            //taskWorker.Wait(token);
            //Task.WaitAll();
            //Task.WaitAny(new[] {t,t2}, 4000, token); //t.status
            try
            {
                Task.WaitAll(taskWorker);
                Console.WriteLine($"result taskWorker is {taskWorker.Result}");
            }
            catch (AggregateException ae)
            {
                // handle exceptions depending on whether they were expected or
                // handles all expected exceptions ('return true'), throws the
                // unhandled ones back as an AggregateException
                ae.Handle(e =>
                {
                    if (e is OperationCanceledException)
                    {
                        Console.WriteLine("Whoops, tasks were canceled.");
                        return true; // exception was handled
                    }
                    else
                    {
                        Console.WriteLine($"Something went wrong: {e}");
                        return false; // exception was NOT handled
                    }
                });
            }
            finally
            {
                // what happened to the tasks?
                Console.WriteLine("\tfaulted\tcompleted\tcancelled");
                Console.WriteLine($"t\t{taskWorker.IsFaulted}\t{taskWorker.IsCompleted}\t{taskWorker.IsCanceled}");
            }
            
        }


        private int taskWorker_DoWork( object objParam, CancellationToken token)
        {
            //objParam
            //Console.WriteLine($" \n Task with id {Task.CurrentId} processing object '{objParam}'...");
            
            
            while (true)
            {
                
                if (token.IsCancellationRequested) // 1. Soft exit
                                                   // RanToCompletion
                {
                    break;
                }
                else
                {
                    
                }
            }


            return 1;
        }
        //**************************************************************************************************************
        //  Background worker handling        
        //**************************************************************************************************************
        private void InitBGWorker(int bgWorkerParam)
        {
            //int maxItems = 50;

            _bgWorkerRx = new BackgroundWorker();
            _bgWorkerRx.WorkerReportsProgress = true;
            _bgWorkerRx.WorkerSupportsCancellation = true;
            _bgWorkerRx.ProgressChanged += bgWorker_ProgressChanged;
            _bgWorkerRx.DoWork += bgWorker_DoWork;
            _bgWorkerRx.RunWorkerCompleted += bgWorker_RunWorkerCompleted;

            _bgWorkerRx.RunWorkerAsync(bgWorkerParam);
        }
        private void bgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            // run all background tasks here
            BackgroundWorker worker = sender as BackgroundWorker;
            int? bgWorkerParam = e.Argument as int?;

            for (int i = 1; i <= bgWorkerParam.GetValueOrDefault(); ++i)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                Thread.Sleep(200);
                worker.ReportProgress(i);

                //item added
            }
        }

        private void bgWorker_RunWorkerCompleted(object sender,
                                               RunWorkerCompletedEventArgs e)
        {

            //update ui once worker complete his work
        }
        void bgWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            double percent = (e.ProgressPercentage * 100) / 50;
            //ProgressBar1.Value = Math.Round(percent, 0);

            //ListBox1.Items.Add(new ListBoxItem { Content = e.ProgressPercentage + " item added" });
            //StatusTextBox.Text = Math.Round(percent, 0) + "% percent completed";
        }

        public int RunSerivceTask(string taskType, int actionParam)
        {
            throw new NotImplementedException();
        }
    }    
}

namespace IntroducingTasks
{
    using System;
    using System.Diagnostics.PerformanceData;
    using System.Threading.Tasks;

    class IntroducingTasks
    {
        public static void Write(char c)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(c);
            }
        }

        public static void Write(object s)
        {
            int i = 1000;
            while (i-- > 0)
            {
                Console.Write(s.ToString());
            }
        }

        public static void CreateAndStartSimpleTasks()
        {
            // a Task is a unit of work in .NET land

            // here's how you make a simple task that does something
            Task.Factory.StartNew(() =>
            {
                //Console.WriteLine("Hello, Tasks!");
                Write('-');
            });

            // the argument is an action, so it can be a delegate, a lambda or an anonymous method

            Task t = new Task(() => Write('?'));
            t.Start(); // task doesn't start automatically!

            Write('.');
        }
/*
        static void Main(string[] args)
        {
            //CreateAndStartSimpleTasks();
            //TasksWithState();
            TasksWithReturnValues();

            Console.WriteLine("Main program done, press any key.");
            Console.ReadKey();
        }
*/
        public static int TextLength(object o)
        {
            Console.WriteLine($"\nTask with id {Task.CurrentId} processing object '{o}'...");
            return o.ToString().Length;
        }

        private static void TasksWithReturnValues()
        {
            string text1 = "testing", text2 = "this";
            var task1 = new Task<int>(TextLength, text1);
            task1.Start();
            var task2 = Task.Factory.StartNew(TextLength, text2);
            // getting the result is a blocking operation!
            Console.WriteLine($"Length of '{text1}' is {task1.Result}.");
            Console.WriteLine($"Length of '{text2}' is {task2.Result}.");
        }

        private static void TasksWithState()
        {
            // clumsy 'object' approach
            Task t = new Task(Write, "foo");
            t.Start();
            Task.Factory.StartNew(Write, "bar");
        }

       
    }

    class CancelingTasks
    {
        /*
        static void Main(string[] args)
        {
            CancelableTasks();
            MonitoringCancelation();
            CompositeCancelationToken();

            Console.WriteLine("Main program done, press any key.");
            Console.ReadKey();
        }
        */
        private static void WaitingForTimeToPass()
        {
            // we've already seen the classic Thread.Sleep

            var cts = new CancellationTokenSource();
            var token = cts.Token;
            var t = new Task(() =>
            {
                Console.WriteLine("You have 5 seconds to disarm this bomb by pressing a key");
                bool canceled = token.WaitHandle.WaitOne(5000);
                Console.WriteLine(canceled ? "Bomb disarmed." : "BOOM!!!!");
            }, token);
            t.Start();

            // unlike sleep and waitone
            // thread does not give up its turn
            Thread.SpinWait(10000);
            Console.WriteLine("Are you still here?");

            Console.ReadKey();
            cts.Cancel();
        }

        private static void CompositeCancelationToken()
        {
            // it's possible to create a 'composite' cancelation source that involves several tokens
            var planned = new CancellationTokenSource();
            var preventative = new CancellationTokenSource();
            var emergency = new CancellationTokenSource();

            // make a token source that is linked on their tokens
            var paranoid = CancellationTokenSource.CreateLinkedTokenSource(
              planned.Token, preventative.Token, emergency.Token);

            Task.Factory.StartNew(() =>
            {
                int i = 0;
                while (true)
                {
                    paranoid.Token.ThrowIfCancellationRequested();
                    Console.Write($"{i++}\t");
                    Thread.Sleep(100);
                }
            }, paranoid.Token);

            paranoid.Token.Register(() => Console.WriteLine("Cancelation requested"));

            Console.ReadKey();

            // use any of the aforementioned token soures
            emergency.Cancel();
        }

        private static void MonitoringCancelation()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;

            // register a delegate to fire
            token.Register(() =>
            {
                Console.WriteLine("Cancelation has been requested.");
            });

            Task t = new Task(() =>
            {
                int i = 0;
                while (true)
                {
                    if (token.IsCancellationRequested) // 1. Soft exit
                                                       // RanToCompletion
                    {
                        break;
                    }
                    else
                    {
                        Console.Write($"{i++}\t");
                        Thread.Sleep(100);
                    }
                }
            });
            t.Start();

            // canceling multiple tasks
            Task t2 = Task.Factory.StartNew(() =>
            {
                char c = 'a';
                while (true)
                {
                    // alternative to what's below
                    token.ThrowIfCancellationRequested(); // 2. Hard exit, Canceled

                    if (token.IsCancellationRequested) // same as above, start HERE
                    {
                        // release resources, if any
                        throw new OperationCanceledException("No longer interested in printing letters.");
                    }
                    else
                    {
                        Console.Write($"{c++}\t");
                        Thread.Sleep(200);
                    }
                }
            }, token); // don't do token, show R# magic

            // cancellation on a wait handle
            Task.Factory.StartNew(() =>
            {
                token.WaitHandle.WaitOne();
                Console.WriteLine("Wait handle released, thus cancelation was requested");
            });

            Console.ReadKey();

            cts.Cancel();

            Thread.Sleep(1000); // cancelation is non-instant

            Console.WriteLine($"Task has been canceled. The status of the canceled task 't' is {t.Status}.");
            Console.WriteLine($"Task has been canceled. The status of the canceled task 't2' is {t2.Status}.");
            Console.WriteLine($"t.IsCanceled = {t.IsCanceled}, t2.IsCanceled = {t2.IsCanceled}");
        }

        private static void CancelableTasks()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;
            Task t = new Task(() =>
            {
                int i = 0;
                while (true)
                {
                    if (token.IsCancellationRequested) // task cancelation is cooperative, no-one kills your thread
                        break;
                    else
                        Console.WriteLine($"{i++}\t");
                }
            });
            t.Start();

            // don't forget CancellationToken.None

            Console.ReadKey();
            cts.Cancel();
            Console.WriteLine("Task has been canceled.");
        }
    }
    class WaitingForTimeToPass
    {
        /*
        static void Main(string[] args)
        {
            // we've already seen the classic Thread.Sleep

            var cts = new CancellationTokenSource();
            var token = cts.Token;
            var t = new Task(() =>
            {
                Console.WriteLine("You have 5 seconds to disarm this bomb by pressing a key");
                bool canceled = token.WaitHandle.WaitOne(5000);
                Console.WriteLine(canceled ? "Bomb disarmed." : "BOOM!!!!");
            }, token);
            t.Start();

            // unlike sleep and waitone
            // thread does not give up its turn
            // avoiding a context switch
            Thread.SpinWait(10000);
            SpinWait.SpinUntil(() => false);
            Console.WriteLine("Are you still here?");

            Console.ReadKey();
            cts.Cancel();

            Console.WriteLine("Main program done, press any key.");
            Console.ReadKey();
        }
        */
    }
    class WaitingForTasks
    {
        /*
        static void Main(string[] args)
        {
            var cts = new CancellationTokenSource();
            cts.CancelAfter(TimeSpan.FromSeconds(3));
            var token = cts.Token;

            var t = new Task(() =>
            {
                Console.WriteLine("I take 5 seconds");
                //Thread.Sleep(5000);

                for (int i = 0; i < 5; ++i)
                {
                    token.ThrowIfCancellationRequested();
                    Thread.Sleep(1000);
                }

                Console.WriteLine("I'm done.");
            });
            t.Start();

            var t2 = Task.Factory.StartNew(() => Thread.Sleep(3000), token);

            //t.Wait();
            //t.Wait(3000);

            // now introduce t2

            //Task.WaitAll(t, t2);
            //Task.WaitAny(t, t2);

            // start w/o token
            try
            {
                // throws on a canceled token
                Task.WaitAll(new[] { t, t2 }, 4000, token);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e);
            }

            Console.WriteLine($"Task t  status is {t.Status}.");
            Console.WriteLine($"Task t2 status is {t2.Status}.");

            Console.WriteLine("Main program done, press any key.");
            Console.ReadKey();
        }
        */
    }
    class ExceptionHandling
    {
        public static void BasicHandling()
        {
            var t = Task.Factory.StartNew(() =>
            {
                throw new InvalidOperationException("Can't do this!") { Source = "t" };
            });

            var t2 = Task.Factory.StartNew(() =>
            {
                var e = new AccessViolationException("Can't access this!");
                e.Source = "t2";
                throw e;
            });

            try
            {
                Task.WaitAll(t, t2);
            }
            catch (AggregateException ae)
            {
                foreach (Exception e in ae.InnerExceptions)
                {
                    Console.WriteLine($"Exception {e.GetType()} from {e.Source}.");
                }
            }
        }

        static void Mainx()
        {
            BasicHandling();

            //      try
            //      {
            //        IterativeHandling();
            //      }
            //      catch (AggregateException ae)
            //      {
            //        Console.WriteLine("Some exceptions we didn't expect:");
            //        foreach (var e in ae.InnerExceptions)
            //          Console.WriteLine($" - {e.GetType()}");
            //      }

            // escalation policy (use Insert Signature CA)
            TaskScheduler.UnobservedTaskException += (object sender, UnobservedTaskExceptionEventArgs args) =>
            {
                // this exception got handled
                args.SetObserved();

                var ae = args.Exception as AggregateException;
                ae?.Handle(ex =>
                {
                    Console.WriteLine($"Policy handled {ex.GetType()}.");
                    return true;
                });
            };

            IterativeHandling(); // throws


            Console.WriteLine("Main program done, press any key.");
            Console.ReadKey();
        }

        private static void IterativeHandling()
        {
            var cts = new CancellationTokenSource();
            var token = cts.Token;
            var t = Task.Factory.StartNew(() =>
            {
                while (true)
                {
                    token.ThrowIfCancellationRequested();
                    Thread.Sleep(100);
                }
            }, token);

            var t2 = Task.Factory.StartNew(() =>
            {
                throw null;
            });

            cts.Cancel();

            try
            {
                Task.WaitAll(t, t2);
            }
            catch (AggregateException ae)
            {
                // handle exceptions depending on whether they were expected or
                // handles all expected exceptions ('return true'), throws the
                // unhandled ones back as an AggregateException
                ae.Handle(e =>
                {
                    if (e is OperationCanceledException)
                    {
                        Console.WriteLine("Whoops, tasks were canceled.");
                        return true; // exception was handled
                    }
                    else
                    {
                        Console.WriteLine($"Something went wrong: {e}");
                        return false; // exception was NOT handled
                    }
                });
            }
            finally
            {
                // what happened to the tasks?
                Console.WriteLine("\tfaulted\tcompleted\tcancelled");
                Console.WriteLine($"t\t{t.IsFaulted}\t{t.IsCompleted}\t{t.IsCanceled}");
                Console.WriteLine($"t1\t{t2.IsFaulted}\t{t2.IsCompleted}\t{t2.IsCanceled}");
            }
        }
    }
}

