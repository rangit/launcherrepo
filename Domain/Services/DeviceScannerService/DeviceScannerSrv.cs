﻿using System;
using System.Collections.Generic;
using LauncherMvvmLight.Model;
using System.Threading.Tasks;


namespace LauncherMvvmLight.Domain.Services.DeviceScannerService
{


    /// <summary>
    /// The Interface defining methods for Create Employee and Read All Employees  
    /// </summary>
    public interface IDeviceScannerService
    {
        List<DeviceInfoModel> GetDevices(String getType);
        List<DeviceOnlyInfoModel> GetDevicesOnly();
        List<DeviceInfoModel> GetDevices();
        Task ScanDevicesAsync();
        bool SaveToRegisteryAsync(List<DeviceOnlyInfoModel> devicesList);
        bool ScanDevices();

    }

    /// <summary>
    /// Class implementing IDataAccessService interface and implementing
    /// its methods by making call to the Entities using CompanyEntities object
    /// </summary>
    public class DeviceScannerService : IDeviceScannerService
    {
        // scanner service
        private DeviceScanner deviceScannerSrv;
        //for Registery option
        //private ObservableCollection<DeviceOnlyInfoModel> _devicesOnly;
        private List<DeviceOnlyInfoModel> _devicesOnly;

        //private ObservableCollection<DeviceInfoModel> _devices;
        private List<DeviceInfoModel> _devices;
        //for File option
        //private ObservableCollection<DeviceInfoModel> _devicesFile;
        private List<DeviceInfoModel> _devicesFile;
        //for Excell option
        //private ObservableCollection<DeviceInfoModel> _devicesExcellFile;
        private List<DeviceInfoModel> _devicesExcellFile;
        //for AccessDB(OLE) option
        //private ObservableCollection<DeviceInfoModel> _devicesAccessDB;
        private List<DeviceInfoModel> _devicesAccessDB;
        //for SQL DB option
        //private ObservableCollection<DeviceInfoModel> _devicesSqlDB;
        private List<DeviceInfoModel> _devicesSqlDB;


        //for Registery option
        //public ObservableCollection<DeviceOnlyInfoModel> DevicesOnly
        public List<DeviceOnlyInfoModel> DevicesOnly
        {
            get
            {
                return _devicesOnly;
            }
            set
            {
                _devicesOnly = value;
            }
        }
        //public ObservableCollection<DeviceInfoModel> Devices
        public List<DeviceInfoModel> Devices
        {
            get
            {
                return _devices;
            }
            set
            {
                _devices = value;
            }
        }
        //for File option
        //public ObservableCollection<DeviceInfoModel> DevicesFile
        public List<DeviceInfoModel> DevicesFile
        {
            get
            {
                return _devicesFile;
            }
            set
            {
                _devicesFile = value;
            }
        }
        //for File option
        //public ObservableCollection<DeviceInfoModel> DevicesExcellFile
        public List<DeviceInfoModel> DevicesExcellFile
        {
            get
            {
                return _devicesExcellFile;
            }
            set
            {
                _devicesExcellFile = value;
            }
        }
        //for Access DB option
        //public ObservableCollection<DeviceInfoModel> DevicesAccessDB
        public List<DeviceInfoModel> DevicesAccessDB
        {
            get
            {
                return _devicesAccessDB;
            }
            set
            {
                _devicesAccessDB = value;
            }
        }
        //for SQL DB option - Entity Framework
        //public ObservableCollection<DeviceInfoModel> DevicesSqlDB
        public List<DeviceInfoModel> DevicesSqlDB
        {
            get
            {
                return _devicesSqlDB;
            }
            set
            {
                _devicesSqlDB = value;
            }
        }


        //constructor
        public DeviceScannerService()
        {
            /* BinaryFile based==> initialize device info repository!!!! and get all listed devices !!!!!
            _deviceInfoRepository = new DeviceInfoRepository("devconfig");            
            _devices = new ObservableCollection<DeviceInfoModel>(_deviceInfoRepository.FindAll());
            */
            /* DB based
            DB based==> initialize device info repository!!!! and get all listed devices !!!!!
            var builder = new DbContextOptionsBuilder<DeviceContext>();
            builder.UseInMemoryDatabase("ExcDevicesDB");
            options = builder.Options;
            */
            //Devices = new ObservableCollection<DeviceInfoModel>();
           // DevicesOnly = new ObservableCollection<DeviceOnlyInfoModel>();

            Devices = new List<DeviceInfoModel>();
            DevicesOnly = new List<DeviceOnlyInfoModel>();

            //Initialize scanner
            deviceScannerSrv = DeviceScanner.Instance;

            Devices = deviceScannerSrv.Devices;
            DevicesOnly = deviceScannerSrv.DevicesOnly;


        }

        //public ObservableCollection<DeviceInfoModel> GetDevices(String getType)
        public List<DeviceInfoModel> GetDevices(String getType)
        {
            /*File-based-->get from info repository
            _devices = new ObservableCollection<DeviceInfoModel>(_deviceInfoRepository.FindAll());
            */
            /*DB-based-->get from info repository
            using (var deviceContext = new DeviceContext())
            {
                //Perform data access using the context 
                deviceContext.ExcDeviceDbSet.Add()
            }
            */





            if (getType == "registery")            
                return Devices;
            else if (getType == "file")
                return DevicesFile;
            else if (getType == "excell")
                return DevicesExcellFile;
            else if (getType == "access")
                return DevicesAccessDB;
            else if (getType == "sql")
                return DevicesSqlDB;
            else
            {
                return null;
            }
        }

        //public ObservableCollection<DeviceInfoModel> GetDevices()
        public List<DeviceInfoModel> GetDevices()
        {
            /*File-based-->get from info repository
            _devices = new ObservableCollection<DeviceInfoModel>(_deviceInfoRepository.FindAll());
            */
            /*DB-based-->get from info repository
            using (var deviceContext = new DeviceContext())
            {
                //Perform data access using the context 
                deviceContext.ExcDeviceDbSet.Add()
            }
            */

            return Devices;
            /*
            if (getType == "registery")
                return Devices;
            else if (getType == "file")
                return DevicesFile;
            else if (getType == "excell")
                return DevicesExcellFile;
            else if (getType == "access")
                return DevicesAccessDB;
            else if (getType == "sql")
                return DevicesSqlDB;
            else
            {
                return null;
            }
            */
        }
        //public ObservableCollection<DeviceOnlyInfoModel> GetDevicesOnlyAsync()
        public List<DeviceOnlyInfoModel> GetDevicesOnlyAsync()
        {

            return DevicesOnly;

        }
        //public ObservableCollection<DeviceOnlyInfoModel> GetDevicesOnly()
        public List<DeviceOnlyInfoModel> GetDevicesOnly()
        {
            /*File-based-->get from info repository
            _devices = new ObservableCollection<DeviceInfoModel>(_deviceInfoRepository.FindAll());
            */
            /*DB-based-->get from info repository
            using (var deviceContext = new DeviceContext())
            {
                //Perform data access using the context 
                deviceContext.ExcDeviceDbSet.Add()
            }
            */

            return DevicesOnly;
           
        }

        //public async Task ScanDevicesAsync()
        public async Task ScanDevicesAsync()
        {
            //deviceScannerSrv.ScanModules();
            //Devices = deviceScannerSrv.Devices;
            //DevicesOnly = deviceScannerSrv.DevicesOnly;
            
            await Task.Run(() => deviceScannerSrv.ScanModules());
        }

        public bool SaveToRegisteryAsync(List<DeviceOnlyInfoModel> devicesList)
        {


            return deviceScannerSrv.SaveToRegistry(devicesList);
        }

        public bool ScanDevices()
        {
            return deviceScannerSrv.ScanModules();
            //return deviceScannerSrv.ScanModulesDemo();

        }
    }
}
